import React from "react";

import SearchBar from "./SearchBar.js"
import ProductTable from "./ProductTable.js"






export default class TablaProductosFiltrados extends React.Component{


	constructor(){
		super();

		this.state={
			filtro : null
		}
	}



	filtrarLista(e){
		let filter = e.target.value
		this.setState({
			filtro : filter
		});


	}

	render(){

		return (
				<div>
					<br/>
					
					<h3>CONSULTA DE PRODUCTOS</h3>
					<hr/>
					<br/>
					<SearchBar onChange={this.filtrarLista.bind(this)}/>
					<ProductTable productos = {this.props.store} filter= {this.state.filtro}/>

				</div>
			);
	}
}
