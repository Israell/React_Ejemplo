import React from "react";
import ProductCategoryRow from "./ProductCategoryRow.js"
import ProductRow from "./ProductRow.js"



export default class ProductTable extends React.Component{


	constructor(){
		super();
	}


	render(){


		let filas  = [];
		let ultimaFila = null;
		let indice=0;





		if(this.props.productos!=null && this.props.filter!=null){
			this.props.productos.forEach((producto)=>{
					let filter  = this.props.filter;
					if(producto.name.indexOf(filter)>-1){
						if(producto.category !=ultimaFila){
							filas.push(<br key = {indice++}/>);
							filas.push(<ProductCategoryRow categoria = {producto.category} key ={indice++}/>);
							
						}

						filas.push(<ProductRow nombreProducto = {producto.name} key = {producto.price}/>);
						ultimaFila=producto.category;
					}
					
			});
		}
		else if(this.props.productos!=null){
			this.props.productos.forEach((producto)=>{
					if(producto.category !=ultimaFila){
						filas.push(<br key = {indice++}/>);
						filas.push(<ProductCategoryRow categoria = {producto.category} key ={indice++}/>);

					}
					filas.push(<ProductRow nombreProducto = {producto.name} key = {producto.price}/>);
					ultimaFila=producto.category;
			});

			
		}
		else 
			filas.push(<h1>Cargando</h1>);

		return (
				<div>
					{filas}
				</div>
			);
	}
}


