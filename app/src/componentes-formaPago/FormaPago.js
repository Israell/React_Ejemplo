import React from "react";

import SearchBar from "../componentes/SearchBar.js"
import ProductTable from "../componentes/ProductTable.js"






export default class FormaPago extends React.Component{


	constructor(){
		super();

		this.state={
			filtro : null
		}
	}



	filtrarLista(e){
		let filter = e.target.value
		this.setState({
			filtro : filter
		});


	}

	render(){

		return (
				<div>
					<br/>
					
					<h3>COMPONENTE PARA FORMA DE PAGO</h3>
					<hr/>
					<br/>
					<SearchBar onChange={this.filtrarLista.bind(this)}/>
					<ProductTable productos = {this.props.store} filter= {this.state.filtro}/>

				</div>
			);
	}
}
